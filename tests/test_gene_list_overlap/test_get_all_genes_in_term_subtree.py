import pytest

import pandas as pd
from numpy import uint8, logical_or

from ebs.imports import StringIO


from go_overlap.create_term_subtree_gene_matrix.term_subtree_gene_matrix import \
    get_all_genes_in_term_subtree


def test_get_all_genes_in_term_subtree(gene_term_map, parent_offspring_map,
                                       expected_result):

    actual_result = get_all_genes_in_term_subtree(parent_offspring_map,
                                                  gene_term_map, 1)

    print(actual_result, type(actual_result), actual_result.shape)
    print(expected_result, type(expected_result), expected_result.shape)

    assert expected_result.equals(actual_result)


@pytest.mark.integration
def test_get_all_genes_in_term_subtree_multicore(gene_term_map, parent_offspring_map,
                                       expected_result):

    actual_result = get_all_genes_in_term_subtree(parent_offspring_map,
                                                  gene_term_map, 2)

    print(actual_result, type(actual_result), actual_result.shape)
    print(expected_result, type(expected_result), expected_result.shape)

    assert expected_result.equals(actual_result)




@pytest.fixture
def all_genes_vector():
    return pd.Series(["a", "b", "c", "d"])


@pytest.fixture
def gene_term_map():
    return pd.read_table(StringIO("""GO:0003674   GO:0005739   GO:0005743
1   0   1
1   1   0
0   0   1
1   1   0"""), sep="\s+", dtype=uint8, header=0)


@pytest.fixture
def parent_offspring_map():
    return pd.read_table(StringIO("""Parent  Offspring
GO:0003674   GO:0005739
GO:0003674   GO:0003674
GO:0005739   GO:0005739
GO:0005743   GO:0005743"""),  sep="\s+", header=0)


@pytest.fixture
def expected_result():
    return pd.read_table(StringIO("""GO:0003674  GO:0005739  GO:0005743
0           1           0           1
1           1           1           0
2           0           0           1
3           1           1           0"""), sep="\s+", header=0, dtype=uint8)
