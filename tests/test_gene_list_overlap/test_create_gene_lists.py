import pytest

import pandas as pd

from ebs.imports import StringIO

from numpy import uint8, logical_or
from go_overlap.create_term_subtree_gene_matrix.term_subtree_gene_matrix import \
    create_gene_list_bool_array


def test_create_gene_list_bool_vectors(all_genes_vector, gene_term_map,
                                       expected_result):

    print(expected_result)
    actual_result = create_gene_list_bool_array(all_genes_vector, gene_term_map)

    print(actual_result)
    assert expected_result.equals(actual_result)


@pytest.fixture
def all_genes_vector():
    return pd.Series(["a", "b", "c", "d"])


@pytest.fixture
def expected_result():
    return pd.read_table(StringIO("""GO:0003674   GO:0005739   GO:0005743
1   0   1
1   1   0
0   1   1
1   1   0"""), sep="\s+", dtype=uint8, header=0)


@pytest.fixture
def gene_term_map():
    return pd.read_table(StringIO("""Gene   Term
a   GO:0003674
b   GO:0003674
d   GO:0003674
b	GO:0005739
c	GO:0005739
d	GO:0005739
a	GO:0005743
c	GO:0005743"""), sep="\s+", header=0)
