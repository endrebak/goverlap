import pytest

import pandas as pd

from numpy import uint8, logical_and

from ebs.imports import StringIO

from go_overlap.compute_set_overlaps.set_overlaps import _compute_go_intersections

def test_compute_go_intersections(input_gene_list_vector, gene_list_bool_array,
                                  expected_result):

    actual_result = _compute_go_intersections([input_gene_list_vector,
                                               gene_list_bool_array])
    print(actual_result, type(actual_result))
    print(expected_result, type(expected_result))
    assert actual_result.equals(expected_result)


@pytest.fixture
def input_gene_list_vector():
    s = pd.Series([1, 0, 0, 1])
    s.name = "a"
    return s


@pytest.fixture
def gene_list_bool_array():
    return pd.read_table(StringIO("""GO:0003674   GO:0005739   GO:0005743
1   0   0
1   1   1
0   1   1
1   1   0"""), sep="\s+", dtype=uint8, header=0)


@pytest.fixture
def expected_result():
    return pd.read_table(StringIO("""go_root  a_I_go
GO:0003674   2
GO:0005739   1
GO:0005743   0"""), sep="\s+", header=0, squeeze=True, index_col=0)
