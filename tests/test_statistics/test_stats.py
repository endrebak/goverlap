import pytest

import pandas as pd
from numpy.testing import assert_allclose, assert_almost_equal
from scipy.stats import hypergeom
from ebs.imports import StringIO

from go_overlap.statistics.stats import find_appropriate_statistical_test, \
    compute_go_overrepresentation_analysis, fishers_exact, compute_odds_ratio


def describe_compute_go_overrepresentation_analysis():

    def test_one(hypergeom_df, expected_result_hyperg1):

        result =  compute_go_overrepresentation_analysis(hypergeom_df)

        print(result)
        print(expected_result_hyperg1)

        assert_allclose(result, expected_result_hyperg1, 5)


@pytest.fixture
def hypergeom_df():

    return pd.read_table(StringIO("""a   a_I_go   go_subtree   U
1281   1006   14760    21941
257   150     11186    21941
131   62   1998    5260"""), header=0, sep="\s+")


@pytest.fixture
def expected_result_hyperg1():
    # results from R
    # > phyper(1006-1, 14760, 21941-14760, 1281, lower.tail=F)
    # [1] 3.428332e-20
    # > phyper(150-1, 11186, 21941-11186, 257, lower.tail=F)
    # [1] 0.01006105
    # > phyper(62-1, 1998, 5260-1998, 131, lower.tail=FALSE)
    # [1] 0.01697598

    return pd.read_table(StringIO("""a   a_I_go   go_subtree   U   a_hpgm_go
1281   1006   14760    21941                                       3.428332e-20
257   150     11186    21941                                       0.01006105
131   62   1998    5260   0.01697598"""), header=0, sep="\s+")


@pytest.fixture
def fisher_df():

    return pd.read_table(StringIO("""a   a_I_go   b    b_I_go
12   5   29    2"""), header=0, sep="\s+")


@pytest.fixture
def fisher_expected_result():

    return pd.read_table(StringIO("""a   a_I_go   b    b_I_go   fisher
12   5   29    2    0.01553"""), header=0, sep="\s+")


def test_fisher_exact(fisher_df, fisher_expected_result):

    result = fishers_exact(fisher_df)
    print(result)

    assert_allclose(result, fisher_expected_result, 5)


@pytest.fixture
def expected_result_odds_ratio():

    # results have not been externally verified

    return pd.read_table(StringIO("""a   a_I_go   go_subtree   U   a_odds
1281   1006   14760    21941                                       2.349376
257   150     11186    21941                                       1.188583
131   62   1998    5260   0.760246"""), header=0, sep="\s+")


@pytest.fixture
def oddsratio_df():

    return pd.read_table(StringIO("""a   a_I_go   go_subtree   U
1281   1006   14760    21941
257   150     11186    21941
131   62   1998    5260"""), header=0, sep="\s+")


def test_odds_ratio(expected_result_odds_ratio, oddsratio_df):

    result = compute_odds_ratio(oddsratio_df)
    print(expected_result_odds_ratio)
    print(oddsratio_df)

    assert_allclose(expected_result_odds_ratio, result, 5)


def describe_find_appropriate_statistical_test():


    def test_equal_lists_give_NA():

        actual_result = find_appropriate_statistical_test([["a", "c", "e"],
                                                           ["c", "e", "a"]])
        assert actual_result == "NA"


    def test_subsetted_lists_give_LAP():


        actual_result = find_appropriate_statistical_test([["a", "c", "e"],
                                                           ["c", "e"]])
        assert actual_result == "LAP"


    def test_intersecting_lists_give_LAP():

        actual_result = find_appropriate_statistical_test([["a", "c", "e"],
                                                           ["c", "e", "b"]])
        assert actual_result == "LAP"


    def test_nonintersecting_lists_give_FE():

        actual_result = find_appropriate_statistical_test([["a", "c", "e"],
                                                           [1, 2, 5]])
        assert actual_result == "FE"
