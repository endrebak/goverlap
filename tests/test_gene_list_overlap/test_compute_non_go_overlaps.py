import pytest

import pandas as pd

from go_overlap.compute_set_overlaps.set_overlaps import compute_non_go_overlaps

from ebs.imports import StringIO


def test_compute_non_go_overlaps_a_I_b(a_vector, b_vector, expected_result):

    xa_I_b, _, _ = expected_result
    _, _, a_I_b, _, _ = compute_non_go_overlaps([a_vector, b_vector])

    print(xa_I_b)
    print(a_I_b)

    assert xa_I_b.equals(a_I_b)


def test_compute_non_go_overlaps_a_M_b(a_vector, b_vector, expected_result):

    _, xa_M_b, _ = expected_result
    _, _, _, a_M_b, _ = compute_non_go_overlaps([a_vector, b_vector])

    print(xa_M_b)
    print(a_M_b)

    assert xa_M_b.equals(a_M_b)


def test_compute_non_go_overlaps_b_M_a(a_vector, b_vector, expected_result):

    _, _, xb_M_a = expected_result
    _, _, _, _, b_M_a = compute_non_go_overlaps([a_vector, b_vector])

    print(xb_M_a)
    print(b_M_a)

    assert xb_M_a.equals(b_M_a)


def test_compute_non_go_overlaps_a_only(a_vector):

    a_I_b = compute_non_go_overlaps([a_vector])

    print(a_I_b)

    assert a_I_b[0].equals(a_vector)


@pytest.fixture
def a_vector():
    return pd.Series([1, 0, 0, 1], dtype=bool)


@pytest.fixture
def b_vector():
    return pd.Series([0, 1, 0, 1], dtype=bool)


@pytest.fixture
def expected_result():
    return [pd.Series([1, 1, 0, 1], dtype=bool),
            pd.Series([1, 0, 0, 0], dtype=bool),
            pd.Series([0, 1, 0, 0], dtype=bool)]
