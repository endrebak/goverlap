
import pytest

import pandas as pd

from numpy import uint8

from go_overlap.prepare_input_lists.lists_to_bool_vectors import \
    prepare_gene_vectors_for_computation, turn_input_gene_lists_into_bool_vectors

@pytest.fixture
def all_genes_vector():
    return pd.Series(["a", "b", "c", "d", "e"])

@pytest.fixture
def gene_vector():
    return pd.Series(["a", "c", "z"])


@pytest.fixture
def expected_result():
    return pd.Series([1, 0, 1, 0, 0], dtype=bool)

def describe_prepare_gene_vectors_for_computation():


    def test_one_vector_only_works(all_genes_vector, gene_vector,
                                   expected_result):

        actual_result = prepare_gene_vectors_for_computation([gene_vector],
                                                             all_genes_vector)

        print(actual_result)
        print(expected_result)

        assert actual_result[0].equals(expected_result)



def describe_turn_input_gene_lists_into_bool_vectors():

    def test_regular_input(gene_vector, all_genes_vector, expected_result):

        actual_result = turn_input_gene_lists_into_bool_vectors(gene_vector,
                                                                all_genes_vector)
        assert actual_result.equals(expected_result)
