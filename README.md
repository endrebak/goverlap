# goverlap

goverlap is a command line ontology term enrichment analyzer.

While there exist many similar tools - such as the R library GOstats or the
immensely popular online tool DAVID - goverlap contains an integrated package
not found elsewhere. It

* is open-source
* supports two input lists to find whether there are significant differences in
  term enrichment between experiments
* contains several ontologies, and can easily be extended with more
* is easy to embed in scripts, making it trivial to analyze and compare any number of files
* does not require any setup or manual downloads
* works for all species that have annotations of genes <-> ontology terms

### Upcoming change

I'm not happy with the time it takes to download data from biomart, so I will be
switching to [mygene](http://mygene.info/) within the coming month.

### Changelog

2015-09-03: Enable comparison of lists where one is a subset of another.
            Use four letter abbreviated species names such as `hsap` instead of `hsapiens`.
            Add stderr messages to notify user when we are downloading data.

### History

goverlap is a command line reimplementation of the now defunct web tool `eGOn`
(prev. `GeneTools`) described in the paper
[GeneTools – application for functional annotation and statistical hypothesis testing](http://www.biomedcentral.com/1471-2105/7/470).

### High-level overview

The result of biological studies is often a list of genes that are found to be
differentially expressed in the experimental condition. But to understand your
results, a list of non-sensical gene names is of little help. You want to know
what happened in the experiment.

Luckily, for most genes, several of their biological functions are known. This
knowledge exists in annotations that map gene names to functions.

This makes it possible to turn a list of genes (your results) into the
biological functions that were affected in the experimental condition.

In short, you go from gobbeldygook like this:

```
Tubb2b
Tubb5
Crmp1
# several hundred more gene names...
```

to something a biologist can understand, like

`Neuroactive ligand-receptor interaction` or `Calcium signaling pathway`.

### Detailed explanation

There are tens of thousands of categories of biological functions and processes,
written down in classification schemes called ontologies. Three randomly drawn
examples of such categories are:

`Generation of a long process of a CNS neuron`, `amine transport` and `antigen receptor-mediated signaling pathway`

These all have known sets of genes associated with them.

For each such category, goverlap finds the number of genes `n` in your input
gene list that is represented in the list of genes associated with that
ontological category. Then it computes the probability that `n` genes from your
gene list would be found associated with that ontological category by chance. If
this probability is low, your genes are likely to be involved in whatever
function that ontology category represents.

How goverlap tells you this is shown in the example output below:

```
go_root	ontology	a_hpgm_go_fdr	a_odds	a	a_I_go	go_subtree	U	a_hpgm_go	go_annotation
GO:0005515	MF	2.70577079119e-55	1.04838649913	1281	808	9038	21941	1.80276553481e-59	protein binding
GO:0005488	MF	1.61551420781e-52	1.77893113733	1281	998	12610	21941	2.15272730736e-56	binding
...
```

We see the corrected p-value (third column) is very low for both the shown
categories. This means that your input genes are very likely to be involved in
`protein binding` and the `binding` categories (Note that the actual list would
contain thousands of more rows, one for each ontology category.)

An explanation of each column in the output:

* `go_root` is the category for which we are investigating possible over- or under-representation
* `ontology` is the classification scheme in which the category exists
* `a_hpgm_go_fdr` is the value `a_hpgm_go` corrected for multiple testing with `Benjamini-Hochberg`
* `a_odds` is the odds ratio which tells the degree and magnitude of over- or under-representation
* `a` is the number of genes in the input gene list
* `a_I_go` is the number of genes both in the input and associated with the category
* `go_subtree` is the number of genes associated with the category
* `U` is the total number of all genes used in the experiment
* `a_hpgm_go` is the p-value for the probability of the input genes being
  overrepresented in the category, computed with the hypergeometric test
* `go_annotation` is a description of the category

#### Two input files

goverlap accepts two input gene lists. In this case it computes the statistics
listed above for each input file. In addition, it also computes the probability
that there is a statistically significant difference in the degree the gene
lists overlap with each ontology category.

The two-list output looks like this:

```
go_root	ontology	chi_sq_fdr	a_hpgm_go_fdr	a_odds	b_hpgm_go_fdr	b_odds	a_I_b_I_go	a_M_b_I_go	b_M_a_I_go	go_subtree	U	a_I_b	a_M_b	b_M_a	chi_sq	test_obs	b_hpgm_go	a_hpgm_go	go_annotation
GO:0034645	BP	1.41394886599e-08	8.70152198131e-49	0.2932375325	8.15535718647e-09	0.278519086818	104	744	53	3645	21941	4238	2957	277	6.10917417739e-11	42.7852281527	1.05412705322e-10	4.92790571265e-51	cellular macromolecule biosynthetic process
GO:0048869	BP	3.54074723275e-08	3.39846246036e-84	0.284942333803	3.4947899252e-22	0.289762955126	105	739	67	3169	21941	4238	2957	277	1.56238156988e-10	40.9491974681	7.21823490446e-25	6.79284921119e-87	cellular developmental process
...
```

An explanation of each column in the output that does not exist in the single
list case:

* `chi_sq_fdr` is the value `chi_sq` corrected for multiple testing
* `a_I_b_I_go` is the number of genes in the intersection of gene list a and b
and the ontology category
* `a_M_b_I_go` is the number of genes in the intersection of gene list a and the
ontology category, but not in gene list b
* `b_M_a_I_go` is the number of genes in the intersection of gene list b and the
ontology category, but not in gene list a
* `a_M_b` is the number of genes in gene list a, but not in gene list b
* `b_M_a` is the number of genes in gene list b, but not in gene list a
* `chi_sq` the probability that the genes from the category are equally likely to
  come from list A or B
* `test_obs` is the test statistic

Note that the statistical test used for two-list comparisons is different,
depending on their set-theoretical relationship. There are four different cases:

##### The lists are overlapping

In this case the Leisering, Alonzo and Pepe test (LAP) is used, since Fisher's
exact is inappropriate when the categories are not mutually exclusive.

##### The lists are mututally exclusive

In this case Fisher's Exact is used.

##### One list is a proper subset of the other

This is an exceedingly rare case, where a version of Fisher's Exact is used. One list
being a proper subset of another is expected only to happen in artificial
situations, where the lists represent the same results, only with different
p-value cutoffs.

When Fisher's Exact test is used, the columns `chi_sq_fdr`, `chi_sq`, `test_obs`
are replaced with `fisher_fdr` and `fisher`.

##### The lists are exactly equal

In the case of equal lists, no comparison is done and goverlap executes as if
only one list was input.

# Install

```fish
$ pip install goverlap
```

# Command line interface

```
goverlap

goverlap is a modern ontology term enrichment analyzer
(Visit github.com/endrebak/goverlap for examples, docs and help.)

Usage:
    goverlap --genes-a=A [--genes-b=B] [--experiment=EXP] [--species=SPE] [--ontologies=ONT] [--limit=LIM] [--ncpus=CPU] [--verbosity=VRB]
    goverlap --help

Arguments:
    -a A --genes-a=A         newline-separated list of the genes in experiment A

Options:
    -h --help                show this message
    -b B --genes-b=B         newline-separated list of the genes in experiment B
    -s SPE --species=SPE     species to do analysis on (examples: hsap, rnor, mmus) [default: hsap]
    -o ONT --ontologies=ONT  comma-separated list of ontologies [default: CC,MF,BP,KEGG]
    -l LIM --limit=LIM       remove categories where the number of genes are more than LIM percent of all genes [default: 0.25]
    -x EXP --experiment=EXP  newline-separated list of all the genes in the experiment
    -v VRB --verbosity VRB   options: quiet, info, debug [default: info]
    -n CPU --ncpus=CPU       number of cores used [default: 1]

Note:
    The gene names need to be of the kind called "external_gene_name"/"associated gene name" in BioMart.
    If they are in any other format, you need to use a BioMart id converter to convert them to this format.
    (see BioMart.org or github.com/endrebak/biomartian)
```

# Example

##### Input files

These example files can be found in the `examples` directory (goverlap does not care about the case of the gene names).

```fish
$ head -3 examples/Simes_Binner_ECIIvsDeep.txt
Ablim2
Elp5
Vmac
$ head -3 examples/FeatureCount_ECIIvsDeep.txt
Slc12a4
Lcat
Tle4
```

This is how we invoke goverlap:

```
$ goverlap -a examples/FeatureCount_ECIIvsDeep.txt \ # first input list
           -b examples/Simes_Binner_ECIIvsDeep.txt \ # second input list
           -s rnor \ # the species name; used for gene and ontology lookups
           -v debug \ # how much output about the running process to print to stderr
           -n 30 -o CC,KEGG \ # which ontologies to use
           -l 0.10 \ # remove any ontology category where the number of associated genes is
                   \ # more than 10% of the total number of genes.
           -x examples/experiment_genes.txt # restrict the genes considered to this list
```

The final output is prited to stdout, and looks like the following:

```fish
# /local/home/endrebak/anaconda/bin/goverlap -a examples/FeatureCount_ECIIvsDeep.txt -b examples/Simes_Binner_ECIIvsDeep.txt -s rnor -v debug -n 30 -o CC -l 0.1 -x examples/experiment_genes.txt
go_root	ontology	chi_sq_fdr	a_hpgm_go_fdr	a_odds	b_hpgm_go_fdr	b_odds	a_I_b_I_go	a_M_b_I_go	b_M_a_I_go	go_subtree	U	a_I_b	a_M_b	b_M_a	chi_sq	test_obs	b_hpgm_go	a_hpgm_go	go_annotation
GO:0030425	CC	1.0	0.00106220302188	0.0298887987807	4.38615591344e-11	0.0486318550661	184	84	7	369	15945	6170	4073	131	0.812241491577	0.056423046715	4.17397866465e-13	4.04327875102e-05	dendrite
GO:0000813	CC	1.0	1.0	0.000165707425424	1.0	0.0	1	1	0	8	15945	6170	4073	131	0.999817133277	5.25278048071e-08	1.0	0.977832062734	ESCRT I complex
GO:0034098	CC	1.0	1.0	0.00033131444239	1.0	0.000477043964417	2	1	0	6	1594	6170	4073	131	0.999741387409	1.05055603884e-07	0.5709446665	0.732240355817	VCP-NPL4-UFD1 AAA ATPase complex
GO:0034666	CC	1.0	0.764751382072	0.00033118065905	0.163919773429	0.000953743443014	2	0	0	2	15945	6170	4073	131	1.0	0.0	0.0172889213846	0.143428788152	integrin alpha2-beta1 complex
GO:0033256	CC	1.0	0.151494176394	0.000993642284264	0.341238515528	0.00143102851765	6	3	0	7	15945	6170	4073	131	0.99865642858	2.83557932883e-06	0.0527688426075	0.0139360225707	I-kappaB/NF-kappaB complex
GO:0000308	CC	1.0	1.0	0.000165607047377	0.866792851338	0.000476906160138	1	0	0	2	15945	6170	4073	131	1.0	0.0	0.245740241359	0.614050045336	cytoplasmic cyclin-dependent protein kinase holoenzyme complex
GO:0044308	CC	1.0	1.0	0.000165607047377	1.0	0.0	1	1	0	2	15945	6170	4073	131	0.999817133277	5.25278048071e-08	1.0	0.614050045336	axonal spine
GO:0032591	CC	1.0	0.298669544542	0.000828035236887	0.262160598469	0.00143092515697	5	2	0	6	15945	6170	4073	131	0.999182261767	1.05038530285e-06	0.0333677843412	0.031974931414	dendritic spine membrane
...
```

# Requirements

Tested on both `python` versions `2.7` and `3.4`.

All the required libraries are installed automatically when fetching goverlap
from PyPI.

```fish
pip install goverlap # also installs pandas, numpy/scipy, joblib, godb, biomartian, kg, ebs
```

To run the tests you also need `py.test`.

# Tests

`sh tests/run_unit_tests.sh` and `sh tests/run_integration_tests.sh`.

Report any failing tests on the issues page.

# Papers or studies using goverlap

Several high-profile papers to be added (will probably take years, though.)

# Todo

* Enable downloading required data ahead of time with a `--get-data` flag. Example: `goverlap --get-data -s rnor -o KEGG,CC`. Currently, goverlap might take surprisingly long to run (the first time), due to having to download data.
* Move cache directory to `~/.goverlap` instead of where the script is istalled (bad, bad me.)

# Contributing

* Make feature or doc requests
* Improve the docs
* Report bugs
* Write tests

# Issues

Please use the go-overlap [issues page](https://github.com/endrebak/go-overlap/issues) for issues, suggestions, feature-requests and troubleshooting and discussing any planned pull-requests that are not merely trivial bug-fixes.

# Further reading

* Clara-Cecilie Gunther, Mette Langaas and Stian Lydersen, 2006: [Statistical Hypothesis Testing of Association Between Two Lists of Genes for a Given Gene Class](http://cds.cern.ch/record/975795/files/cer-002639406.pdf?version=1)
* Beisvåg et. al. 2006: [GeneTools – application for functional annotation and statistical hypothesis testing](http://www.biomedcentral.com/1471-2105/7/470) (The Leisering test is described there as "the intersecting target-target situation")

# Cite

If you use this tool for a publication, please cite the Beisvåg (2006) paper above and include a link to this repository.

# Thanks

* Vidar Beisvåg et. al. for the eGOn suite goverlap was based on
* Mette Langaas for statistics help
* Pål Sætrom for the idea and supervision

# Technical notes

##### Memory usage

goverlap represents the genes in each ontology category as a vector of bools. Only the genes in any of the input lists need to be considered (for most computations). This means that if there are 500 genes in the union of the input lists and there are 15k ontology categories,
the RAM required should be less than that of the Python interpreter (15000 * 500 * 8 bits). Hence, if you experience excessive memory consumption, this is a bug.

# Recipies

##### Convert the genes in your input files to the common type sometimes called "associated gene name" or "external gene name".

The gene names we need to convert look like the following:

```
head -5 examples/mrna_genes.txt
Gene
NM_001001504
NM_001001508
NM_001001512
NM_001001514
```

To convert them, we need to find out what gene identifiers with `NM_` in their names are called:

```
biomartian -d rnorvegicus_gene_ensembl --list-attributes | grep 'NM_'
refseq_dna	RefSeq DNA ID [e.g. NM_203373]
refseq_mrna	RefSeq mRNA [e.g. NM_001195597]
```

`refseq_mrna` is what looks the most like what we have. So we request the `external_gene_name` for our `refseq_mrna` and tell biomartian to look for the gene names to convert in column 0 and voilà!

```
biomartian -d rnorvegicus_gene_ensembl -i refseq_mrna -o external_gene_name -c 0 examples/mrna_genes.txt | cut -f 2 | head -5
external_gene_name
Gtf2ird1
Lppr4
Gtf2i
Ablim2
```

### Similar software

Feel free to add to this list.

* [goatools](https://github.com/tanghaibao/goatools) (Python)
* [GOstats](http://www.bioconductor.org/packages/release/bioc/html/GOstats.html) (R)
* [DAVID](https://david.ncifcrf.gov/) (Online - NB: outdated annotations!)
