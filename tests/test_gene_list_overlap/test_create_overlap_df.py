import pytest

import pandas as pd
from numpy import array_equal

from ebs.imports import StringIO

from go_overlap.compute_set_overlaps.set_overlaps import compute_go_overlap_matrix
from go_overlap.compute_set_overlaps.set_overlaps import find_nb_go_subtree_genes

""" Computes all gene list overlaps. Should test to see if works with one and two
input vectors."""

@pytest.mark.integration
def describe_create_overlap_df():

    def test_create_overlap_df(gene_list_bool_array, gene_lists, expected_result):
        actual_result = compute_go_overlap_matrix(gene_list_bool_array, gene_lists, 2)
        print(actual_result, type(actual_result), actual_result.shape, "actual")
        print(expected_result, type(expected_result), expected_result.shape, "expected")

        assert actual_result.equals(expected_result)


    def test_multicore_equals_singlecore(gene_list_bool_array, gene_lists, expected_result):
        actual_result = compute_go_overlap_matrix(gene_list_bool_array, gene_lists, 1)
        print(actual_result, type(actual_result), "actual")
        print(expected_result, type(expected_result), "expected")

        assert actual_result.equals(expected_result)

# These a_I_b is not equal to a intersect b and so on but that is not important because those computations
# are tested elsewhere
@pytest.fixture
def gene_lists():
    return [pd.Series([1, 0, 1, 0, 1], name="a", dtype=bool),
            pd.Series([1, 0, 1, 1, 0], name="b", dtype=bool),
            pd.Series([1, 0, 1, 0, 1], name="a_I_b", dtype=bool),
            pd.Series([1, 0, 0, 0, 0], name="a_M_b", dtype=bool),
            pd.Series([0, 0, 1, 0, 0], name="b_M_a", dtype=bool)]


@pytest.fixture
def gene_list_bool_array():
    return pd.read_table(StringIO("""GO:0003674   GO:0005739   GO:0005743    GO:0005744
1   0   0   0
1   1   1   0
1   1   1   0
0   1   1   0
1   1   0   0"""), sep="\s+", dtype=bool, header=0)


@pytest.fixture
def expected_result():
    return pd.read_table(StringIO("""go_root a_I_go  b_I_go  a_I_b_I_go  a_M_b_I_go  b_M_a_I_go  a  b  a_I_b  a_M_b  b_M_a  U  go_subtree
GO:0003674   3  2  3     1   1  3  3  3   1    1   5   4
GO:0005739   2  2  2     0   1  3  3  3   1    1   5   4
GO:0005743   1  2  1     0   1  3  3  3   1    1   5   3
GO:0005744   0  0  0     0   0  3  3  3   1    1   5   0"""), sep="\s+", header=0,
                         squeeze=True)


@pytest.fixture
def expected_nb_go_subtree_genes():
    return pd.read_table(StringIO("""GO:0003674    4
GO:0005739    4
GO:0005743    3
GO:0005744    0"""), sep="\s+", header=None, squeeze=True, index_col=0)


def test_nb_go_subtree_genes(gene_list_bool_array, expected_nb_go_subtree_genes):

    nb_go_subtree_genes = find_nb_go_subtree_genes(gene_list_bool_array)

    print(nb_go_subtree_genes)
    print(expected_nb_go_subtree_genes)

    assert nb_go_subtree_genes.equals(expected_nb_go_subtree_genes)
